name := "graal4s"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {

  lazy val http4sVersion = "0.20.8"
  lazy val circeVersion = "0.9.1"
  lazy val circeConfigVersion = "0.6.1"
  lazy val circeGenericVersion = "0.11.1"

  Seq(
    "org.http4s"            %% "http4s-blaze-server"    % http4sVersion,
    "org.http4s"            %% "http4s-circe"           % http4sVersion,
    "org.http4s"            %% "http4s-dsl"             % http4sVersion,
    "io.circe"              %% "circe-core"             % circeVersion,
    "io.circe"              %% "circe-generic"          % circeVersion,
    "io.circe"              %% "circe-config"           % circeConfigVersion,
    "io.circe"              %% "circe-generic-extras"   % circeGenericVersion,
    "org.codehaus.janino"   % "janino"                  % "3.1.0"
  )

}

resolvers ++= Seq(
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)
