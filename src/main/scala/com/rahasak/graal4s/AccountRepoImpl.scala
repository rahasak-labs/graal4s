package com.rahasak.graal4s

import cats.effect.IO

class AccountRepoImpl() extends AccountRepo {
  override def createAccount(account: Account) = {
    for {
      s <- IO {
        "1"
      }
    } yield s
  }

  override def updateAccount(id: String, account: Account) = {
    IO {
      1
    }
  }

  override def getAccount(id: String) = {
    IO {
      Option(Account("1", "ops", 2323232))
    }
  }

  override def getAccounts() = {
    IO {
      List()
    }
  }
}


