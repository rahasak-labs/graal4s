package com.rahasak.graal4s

import cats.data.Kleisli
import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import fs2.Stream
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.{Request, Response}

object Main extends IOApp {

  def makeRouter(): Kleisli[IO, Request[IO], Response[IO]] = {
    println("make route - ---")
    Router[IO](
      "/api/v1" -> AccountRoutes.routes(new AccountRepoImpl())
    ).orNotFound
  }

  def serveStream(serverConfig: ServerConfig): Stream[IO, ExitCode] = {
    println("server stream ---")
    BlazeServerBuilder[IO]
      .bindHttp(serverConfig.port, serverConfig.host)
      .withHttpApp(makeRouter())
      .serve
  }

  override def run(args: List[String]): IO[ExitCode] = {
    println("running ---")
    val stream = for {
      config <- Stream.eval(Config.load())
      exitCode <- serveStream(config.serverConfig)
    } yield exitCode

    stream.compile.drain.as(ExitCode.Success)
  }

}
